#!/usr/bin/env bash

[[ "${PWD##*/}" != "mapview" ]] && echo "ERROR: You must launch the script from within the mapview's folder."

npm -v &> /dev/null || (echo "Installing node & npm..." && curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash - &> /dev/null && sudo apt-get install -y nodejs &> /dev/null)

if ! goaccess -V &> /dev/null
then
    echo "Installing Goaccess..."

    git clone https://github.com/allinurl/goaccess.git &> /dev/null
    cd goaccess || return
    git checkout 1ab70d7 &> /dev/null
    sudo apt install autoconf autopoint gettext libtokyocabinet-dev zlib1g-dev libbz2-dev libssl-dev -y &> /dev/null
    sudo autoreconf -fiv &> /dev/null
    ./configure --enable-geoip --enable-utf8 --with-openssl --enable-tcb=btree &> /dev/null
    make &> /dev/null
    sudo make install &> /dev/null
    mkdir -p analytics/db
    cd ..
fi

echo "Launching Goaccess..."

id_go="$( ps aux | grep 'goaccess' | grep 'mapview' | awk '{print $2}' | head -1 )" &> /dev/null

[[ -n "$id_go" ]] && sudo kill -9 "$id_go" &> /dev/null

sudo goaccess \
    -f /var/log/nginx/access.log \
    -o /home/ubuntu/mapview/analytics/report.html \
    --real-time-html \
    --log-format=COMBINED \
    --date-format=%d/%b/%Y \
    --time-format=%T \
    --ssl-cert=/etc/letsencrypt/live/mapview.rincent-ndt.fr/cert.pem \
    --ssl-key=/etc/letsencrypt/live/mapview.rincent-ndt.fr/privkey.pem \
    --ws-url=mapview.rincent-ndt.fr \
    --load-from-disk \
    --keep-db-files \
    --db-path /home/ubuntu/mapview/analytics/db \
    --real-os \
    --ignore-crawlers \
    --anonymize-ip \
    --daemonize \
    &> /dev/null

while :
do
    echo "Checking if can deploy..."
    is_already_deploying="$( ps aux | grep 'deploy.sh' | grep -c 'bash' )"
    is_already_deploying=$(( is_already_deploying - 2 ))

    [[ $is_already_deploying -lt 1 ]] && echo "Continuing..." && break

    echo "WARNING: Already deploying !"
    echo "Waiting 5s..."
    sleep 5
done

echo "Archiving current codebase..."
backup="backups/backup-$( date +%F_%H-%M-%S )"
mkdir -p "$backup"
rsync -a . "$backup" --exclude backups --exclude backups

echo "Pulling from Gitlab..."
git pull &> /tmp/git || (cat /tmp/git && rm -rf "$backup" && exit 1)

mkdir -p logs
log="logs/log-$( git log --format="%H" -n 1 )"

echo "Updating the system..."
{
    sudo apt update -y;
    sudo apt upgrade -y;
    sudo npm update -g;
} &>> "$log"

yarn -v &> /dev/null || (echo "Installing yarn..." && sudo npm install -g yarn &> /dev/null)

need_yarn_install="$( git diff --name-only HEAD~1 HEAD | grep package.json )"

[[ -n "$need_yarn_install" ]] && echo "Installing packages..." && yarn install &>> "$log"

echo "Building..."
yarn prod:build &>> "$log"

while :
do
    echo "Checking if can run the server..."
    id_serve="$( ps aux | grep 'serve -s dist' | grep 'node' | awk '{print $2}' | head -1 )"

    [[ -z "$id_serve" ]] && echo "Continuing..." && break

    echo "WARNING: An instance of the server is already running !"
    echo "Killing the instance..."
    sudo kill -9 "$id_serve" &>> /dev/null
done

nohup yarn prod:start >> "$log" 2>&1 &

echo "backup: ${backup}" &>> "$log"

echo "Done."
echo ""
echo "You can find the logs in ${log}"
echo "and a backup of the previous versions in ${backup}"
