process.env.VUE_APP_VERSION = require('./package.json').version

module.exports = {
  chainWebpack: (config) => {
    const svgRule = config.module.rule('svg')
    svgRule.uses.clear()
    svgRule.use().loader('svg-url-loader?noquotes')
  },
  lintOnSave: false,
  pwa: {
    manifestOptions: {
      name: 'Mapview Reporting',
      description: `An application that lets you import and visualize data from Rincent ND Technologies' machines and lets you create custom Excel reports.`,
      start_url: 'https://v1.mapview.fr',
    },
    workboxOptions: {
      skipWaiting: true,
      clientsClaim: true,
    },
  },
}
