module.exports = {
    resolve: {
        extensions: [
            '.js',
            '.jsx',
            '.ts',
            '.tsx',
            '.vue',
            '.json',
            '/index.ts',
            '/index.vue',
        ],
    },
};
